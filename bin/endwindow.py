#! /usr/bin/env python
# -*- coding: utf-8 -*-
import os
import pygame
import time
import sys
from main import *

from pygame.locals import *

pygame.init()
pygame.display.set_caption("飞机大战--16软件五班软工一组作品")  # 设置标题
screen=pygame.display.set_mode((480,850),0,32)  # 设置窗口大小
pygame.display.set_mode((480,850))
background=pygame.image.load(r"material/startwindow.png")
logoimage=pygame.image.load(r"material/logoimage.png")
def main3(djpoint):
    texts = '您的最后得分%s'%djpoint
    font = pygame.font.SysFont('SimHei', 32, True)  # 设置使用系统字体（用于支持中文）
    center, middle = screen.get_rect().center  # 获取屏幕中心坐标值
    antialias = True  # 设置抗锯齿（平滑文字）
    black = 0, 0, 0  # 设置文字颜色
    while 1:
        screen.blit(background,(0,0))
        screen.blit(logoimage,(30,90))
        for event in pygame.event.get():
            pygame.display.update()
            time.sleep(0.01)
            if event.type == pygame.QUIT:
                    pygame.quit()
                    sys.exit()

            if event.type == pygame.MOUSEBUTTONDOWN:
                    pygame.quit()
                    sys.exit()
        
        text = font.render(u'%s'%texts, antialias, black)  # 创建单行文字对象
        text2 = font.render(u'感谢您游玩本程序点击来退出！', antialias, black)  # 创建单行文字对象
        screen.blit(text, (120, 350))# 屏幕填充文字
        screen.blit(text2, (5, 450))# 屏幕填充文字
 

        pygame.display.flip()     # 刷新屏幕

##if __name__=="__main__":
##    main3(300)
