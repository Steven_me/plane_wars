#! /usr/bin/env python
# -*- coding: utf-8 -*-

"""
    定义敌机
"""

from random import randint
from pygame.locals import *
import pygame


class SmallEnemy(pygame.sprite.Sprite):
    """
    定义小飞机敌人
    """
    energy = 25

    def __init__(self, bg_size):
        super(SmallEnemy, self).__init__()
        self.image = pygame.image.load("material/image/enemy1.png")
        self.rect = self.image.get_rect()
        self.width, self.height = bg_size[0], bg_size[1]
        self.mask = pygame.mask.from_surface(self.image)  # 获取飞机图像的掩膜用以更加精确的碰撞检测
        self.speed = 2
        self.energy = SmallEnemy.energy
        # 定义敌机出现的位置, 保证敌机不会在程序已开始就立即出现
        self.rect.left, self.rect.top = (
            randint(0, self.width - self.rect.width),  randint(-5 * self.rect.height, -5),
        )
        self.active = True
        # 加载飞机损毁图片
        self.destroy_images = []
        self.destroy_images.extend(
            [
                pygame.image.load("material/image/enemy1_down1.png"),
                pygame.image.load("material/image/enemy1_down2.png"),
                pygame.image.load("material/image/enemy1_down3.png"),
                pygame.image.load("material/image/enemy1_down4.png")
            ]
        )
##    def xueliang(self,eachp,each,screen):
##        #打印血量条
##         color_black = (0, 0, 0)
##         color_green = (0, 255, 0)
##         color_red = (255, 0, 0)
##         color_white = (255, 255, 255)
##         energy_remain = eachp / SmallEnemy.energy
##         if 1 >= energy_remain > 0.2:  # 如果血量大约百分之二十则为绿色，否则为红色
##                    energy_color = color_green
##         if 0.2 >= energy_remain > 0:
##                     energy_color = color_red
##         if energy_remain <=0:
##                      each = 25
##         pygame.draw.line(screen, energy_color,
##                         (each.rect.left, each.rect.top - 5),
##                         (each.rect.left + each.rect.width * energy_remain, each.rect.top - 5),
##                             2)
##         return eachp

    def move(self):
        """
        定义敌机的移动函数
        :return:
        """
        if self.rect.top < self.height:
            self.rect.top += self.speed
        else:
            self.reset()
        

    def reset(self):
        """
        当敌机向下移动出屏幕且飞机是需要进行随机出现的, 以及敌机死亡
        :return:
        """
        self.rect.left, self.rect.top = (randint(0, self.width - self.rect.width), randint(-5 * self.rect.height, 0))
        self.active = True


class MidEnemy(pygame.sprite.Sprite):
    energy = 100
    def __init__(self, bg_size):
        super(MidEnemy, self).__init__()
        self.image = pygame.image.load("material/image/enemy2.png")
        self.rect = self.image.get_rect()
        self.width, self.height = bg_size[0], bg_size[1]
        self.mask = pygame.mask.from_surface(self.image)  # 获取飞机图像的掩膜用以更加精确的碰撞检测
        self.speed = 1
        self.energy = MidEnemy.energy
        # 定义敌机出现的位置, 保证敌机不会在程序已开始就立即出现
        self.rect.left, self.rect.top = (
            randint(0, self.width - self.rect.width),  randint(-5 * self.rect.height, -5),
        )
        self.active = True
        # 加载飞机损毁图片
        self.destroy_images = []
        self.destroy_images.extend(
            [
                pygame.image.load("material/image/enemy2_down1.png"),
                pygame.image.load("material/image/enemy2_down2.png"),
                pygame.image.load("material/image/enemy2_down3.png"),
                pygame.image.load("material/image/enemy2_down4.png")
            ]
        )
    def move(self):
        """
        定义敌机的移动函数
        :return:
        """
        if self.rect.top < self.height:
            self.rect.top += self.speed
        else:
            self.reset()
        

    def reset(self):
        """
        当敌机向下移动出屏幕且飞机是需要进行随机出现的, 以及敌机死亡
        :return:
        """
        self.rect.left, self.rect.top = (randint(0, self.width - self.rect.width), randint(-5 * self.rect.height, 0))
        self.active = True


class BigEnemy(pygame.sprite.Sprite):
    energy = 100
    def __init__(self,bg_size):
        super(BigEnemy, self).__init__()
        self.image = pygame.image.load("material/image/enemy3.png")
        self.rect = self.image.get_rect()
        self.width, self.height = bg_size[0], bg_size[1]
        self.mask = pygame.mask.from_surface(self.image)  # 获取飞机图像的掩膜用以更加精确的碰撞检测
        self.speed = 0.8
        self.energy = BigEnemy.energy
        # 定义敌机出现的位置, 保证敌机不会在程序已开始就立即出现
        self.rect.left, self.rect.top = (
            randint(0, self.width - self.rect.width),  randint(-5 * self.rect.height, -5),
        )
        self.active = True
        # 加载飞机损毁图片
        self.destroy_images = []
        self.destroy_images.extend(
            [
                pygame.image.load("material/image/enemy3_down1.png"),
                pygame.image.load("material/image/enemy3_down2.png"),
                pygame.image.load("material/image/enemy3_down3.png"),
                pygame.image.load("material/image/enemy3_down4.png")
            ]
        )
    def move(self):
        """
        定义敌机的移动函数
        :return:
        """
        if self.rect.top < self.height:
            self.rect.top += self.speed
        else:
            self.reset()
        

    def reset(self):
        """
        当敌机向下移动出屏幕且飞机是需要进行随机出现的, 以及敌机死亡
        :return:
        """
        self.rect.left, self.rect.top = (randint(0, self.width - self.rect.width), randint(-5 * self.rect.height, 0))
        self.active = True


