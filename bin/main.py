#! /usr/bin/env python
# -*- coding: utf-8 -*-
#系统拥有的模块
import os
import sys
import pygame
from pygame.locals import *

#个人定制模块
from mapp import *
from endwindow import *
from settings import *
from state import *
from enemy import *
from plane import OurPlane  # 导入我们的飞机
from bullet import Bullet



pygame.mixer.init()
pygame.init()
bg_size = 480, 850  # 初始化游戏背景大小(宽, 高)
##screen = pygame.display.set_mode(bg_size)  # 设置背景对话框
##pygame.display.set_caption("飞机大战")  # 设置标题
##background = pygame.image.load( "material/image/background.png")  # 加载背景图片,并设置为不透明
####goinggame = pygame.image.load(os.path.join(BASE_DIR, "material/image/btn_finish.png"))

# 血槽颜色绘制
color_black = (0, 0, 0)
color_green = (0, 255, 0)
color_red = (255, 0, 0)
color_white = (255, 255, 255)

# 获取我方飞机
our_plane = OurPlane(bg_size)

#暂停状态

pause = False
running = True

#地图
mainScene = MainScene()
def add_small_enemies(group1, group2, num):
    """
    添加小型敌机
    指定个敌机对象添加到精灵组（sprite.group）
    参数group1、group2是两个精灵组类型的形参，用以存储多个精灵对象（敌机）。
    需要注意的一点是group既然是特定的精灵组结构体，在向其内部添加精灵对象时需要调用其对应的成员函数add()
    :return:
    """
    for i in range(num):
        small_enemy = SmallEnemy(bg_size)
        group1.add(small_enemy)
        group2.add(small_enemy)
def add_mid_enemy(group1, group2, num):#boss1加组方法
    for i in range(num):
        mid_enemy = MidEnemy(bg_size)
        group1.add(mid_enemy)
        group2.add(mid_enemy)
def add_big_enemy(group1, group2, num):#boss1加组方法
    for i in range(num):
        big_enemy = BigEnemy(bg_size)
        group1.add(big_enemy)
        group2.add(big_enemy)
def paused(pause):#新暂停方法
     pygame.mixer.music.play(1)  # loops 接收该参数, -1 表示无限循环(默认循环播放一次)
     bg_size = 480, 850  # 初始化游戏背景大小(宽, 高)
     screen = pygame.display.set_mode(bg_size)  # 设置背景对话框
     pygame.display.set_caption("飞机大战")  # 设置标题
     clock = pygame.time.Clock() # 微信的飞机貌似是喷气式的, 那么这个就涉及到一个帧数的问题
     clock.tick(60)
     Paused.paused_display(Paused,screen)
     # 响应用户的操作
     while pause:
         pygame.mixer.music.play(0)
         for event in pygame.event.get():
            if event.type == 12:  # 如果用户按下屏幕上的关闭按钮，触发QUIT事件，程序退出
                pygame.quit()
                sys.exit()
         key_pressed = pygame.key.get_pressed()
         if key_pressed[K_ESCAPE]:
                pause = False
                button_down_sound.play()
                pygame.mixer.music.play(-1)
def main(djpoint):
    # 响应音乐
    pygame.mixer.music.play(-1)  # loops 接收该参数, -1 表示无限循环(默认循环播放一次)
    running = True
    switch_image = False  # 切换飞机的标识位(使飞机具有喷气式效果)
    delay = 60  # 对一些效果进行延迟, 效果更好一些
    font = pygame.font.SysFont('SimHei', 20, True)#使用系统字体
    font1 = pygame.font.SysFont('SimHei', 25, True)#使用系统字体
    font2 = pygame.font.SysFont('SimHei', 30, True)#使用系统字体



    enemies = pygame.sprite.Group()  # 生成敌方飞机组(一种精灵组用以存储所有敌机精灵)
    small_enemies = pygame.sprite.Group()  # 敌方小型飞机组(不同型号敌机创建不同的精灵组来存储)
    mid_enemies = pygame.sprite.Group()# 敌方boss1飞机组(不同型号敌机创建不同的精灵组来存储)
    big_enemies = pygame.sprite.Group()# 敌方boss2飞机组(不同型号敌机创建不同的精灵组来存储)

    add_small_enemies(small_enemies, enemies, 6)  # 生成若干敌方小型飞机
    add_mid_enemy(mid_enemies,enemies,1)# 生成1个敌方boss1飞机
    add_big_enemy(big_enemies,enemies,1)# 生成1个敌方boss2飞机

    # 定义子弹, 各种敌机和我方敌机的毁坏图像索引
    bullet_index = 0
    e1_destroy_index = 0
    me_destroy_index = 0
    boss_destroy_index = 0
    #定义积分和状态
##    djpoint = 0
    next_state = 0
    boss1_energy = 100
    djsm = 6

    # 定义子弹实例化个数
    bullet1 = []
    bullet_num = 8
    for i in range(bullet_num):
        bullet1.append(Bullet(our_plane.rect.midtop))

    while running:
##        if next_state == 1:
##               pygame.mixer.music.play(0)  # loops 接收该参数, -1 表示无限循环(默认循环播放一次)
##            # 微信的飞机貌似是喷气式的, 那么这个就涉及到一个帧数的问题
##               clock = pygame.time.Clock()
##               clock.tick(60)
##               Paused.paused_display(Paused,screen)
##               # 响应用户的操作
##               for event in pygame.event.get():
##                  if event.type == 12:  # 如果用户按下屏幕上的关闭按钮，触发QUIT事件，程序退出
##                       pygame.quit()
##                       exit()
##                       sys.exit()
##               key_pressed = pygame.key.get_pressed()
##               if key_pressed[K_ESCAPE]:
##                    next_state = 0  # 存储下一个游戏状态
########               time.sleep(0.01)
        global pause

        # 绘制背景图
        mainScene.run_scene()

        # 微信的飞机貌似是喷气式的, 那么这个就涉及到一个帧数的问题
        clock = pygame.time.Clock()
        clock.tick(60)

        # 绘制我方飞机的两种不同的形式
        if not delay % 3:
            switch_image = not switch_image
        #初始化boss
            for boss1 in mid_enemies:
                if boss1.active:
                    pass
                else:
                    if boss_destroy_index == 0:
                        enemy1_down_sound.play()
                    screen.blit(boss1.destroy_images[boss_destroy_index], boss1.rect)
                    boss_destroy_index = (boss_destroy_index + 1) % 4
                    if boss_destroy_index == 0:
                        boss1.reset()
                        boss1_energy = 100
            
        #小敌机出现的条件
        for each in small_enemies:
                    if each.active:
                        # 随机循环输出小飞机敌机
                        each.move()
                        screen.blit(each.image, each.rect)
##                            pygame.draw.line(screen, color_black,
##                                             (each.rect.left, each.rect.top - 5),
##                                             (each.rect.right, each.rect.top - 5),
##                                             2)
##                            energy_remain = each.energy / SmallEnemy.energy
##                            if 1 >= energy_remain > 0.2:  # 如果血量大约百分之二十则为绿色，否则为红色
##                                energy_color = color_green
##                            if 0.2 >= energy_remain > 0:
##                                energy_color = color_red
##                            if energy_remain <=0:
##                                  each.energy = 25
##                                  djsm+=1
##                            pygame.draw.line(screen, energy_color,
##                                             (each.rect.left, each.rect.top - 5),
##                                             (each.rect.left + each.rect.width * energy_remain, each.rect.top - 5),
##                                             2)
                    else:
                        if e1_destroy_index == 0:
                            enemy1_down_sound.play()
                        screen.blit(each.destroy_images[e1_destroy_index], each.rect)
                        e1_destroy_index = (e1_destroy_index + 1) % 4
                        if e1_destroy_index == 0:
                            each.reset()
        #第一个BOSS
        if 600 > djpoint > 500:
            surface3 = font2.render(u'boss1即将到来！', True, [255, 0, 0])
            screen.blit(surface3, [120, 50])
        elif 1000 > djpoint > 500:
            for boss1 in mid_enemies:
                if boss1.active:
                    # 随机循环输出boss1敌机
                    boss1.move()
                    screen.blit(boss1.image, boss1.rect)
                    pygame.draw.line(screen, color_black,
                                     (boss1.rect.left, boss1.rect.top - 2),
                                     (boss1.rect.right, boss1.rect.top - 2),
                                     2)
                    boss1_energy_remain = boss1_energy / MidEnemy.energy
                    if 1 >= boss1_energy_remain > 0.2:  # 如果血量大约百分之二十则为绿色，否则为红色
                        boss1_energy_color = color_green
                    if 0.2 >= boss1_energy_remain > 0:
                        boss1_energy_color = color_red
                    if boss1_energy_remain <=0:
                          boss1_energy = 100
                    pygame.draw.line(screen, boss1_energy_color,
                                     (boss1.rect.left, boss1.rect.top - 2),
                                     (boss1.rect.left + boss1.rect.width * boss1_energy_remain, boss1.rect.top - 2),
                                     2)
                else:
                    if boss_destroy_index == 0:
                        enemy2_down_sound.play()
                        pygame.mixer.music.play(0)
                        er_bgm_sound.play(-1)
                    screen.blit(boss1.destroy_images[boss_destroy_index], boss1.rect)
                    boss_destroy_index = (boss_destroy_index + 1) % 4
                    if boss_destroy_index == 0:
                        boss1.reset()
                        boss1_energy = 100
##                else:
##                        if e1_destroy_index == 0:
##                            enemy1_down_sound.play()
##                        screen.blit(each.destroy_images[e1_destroy_index], each.rect)
##                        e1_destroy_index = (e1_destroy_index + 1) % 4
##                        if e1_destroy_index == 0:
        #第二个boss
        if djpoint > 1500:
                   for boss1 in big_enemies:
                        if boss1.active:
                            # 随机循环输出boss1敌机
                            boss1.move()
                            screen.blit(boss1.image, boss1.rect)
                            pygame.draw.line(screen, color_black,
                                             (boss1.rect.left, boss1.rect.top - 2),
                                             (boss1.rect.right, boss1.rect.top - 2),
                                             2)
                            boss1_energy_remain = boss1_energy / MidEnemy.energy
                            if 1 >= boss1_energy_remain > 0.2:  # 如果血量大约百分之二十则为绿色，否则为红色
                                boss1_energy_color = color_green
                            if 0.2 >= boss1_energy_remain > 0:
                                boss1_energy_color = color_red
                            if boss1_energy_remain <=0:
                                  boss1_energy = 100
                            pygame.draw.line(screen, boss1_energy_color,
                                             (boss1.rect.left, boss1.rect.top - 2),
                                             (boss1.rect.left + boss1.rect.width * boss1_energy_remain, boss1.rect.top - 2),
                                             2)
                        else:
                            if boss_destroy_index == 0:
                                enemy3_down_sound.play()
                            screen.blit(boss1.destroy_images[boss_destroy_index], boss1.rect)
                            boss_destroy_index = (boss_destroy_index + 1) % 4
                            if boss_destroy_index == 0:
                                boss1.reset()
                                boss1_energy = 100      
                        
            
            

        # 当我方飞机存活状态, 正常展示
        if our_plane.active:
            if switch_image:
                screen.blit(our_plane.image_one, our_plane.rect)
            else:
                screen.blit(our_plane.image_two, our_plane.rect)

            # 飞机存活的状态下才可以发射子弹
            if not (delay % 10):  # 每十帧发射一颗移动的子弹
                
                bullet_sound.play()
                bullets = bullet1
                bullets[bullet_index].reset(our_plane.rect.midtop)
                bullet_index = (bullet_index + 1) % bullet_num
                bullet_sh = -5
                

            for b in bullets:
                if b.active:  # 只有激活的子弹才可能击中敌机
                    b.move()
                    screen.blit(b.image, b.rect)
                    enemies_hit = pygame.sprite.spritecollide(b,small_enemies, False, pygame.sprite.collide_mask)
                    
                    if enemies_hit:# 如果子弹击中飞机
                        b.active = False# 子弹损毁
                        djpoint+=10
##                            if each.energy == 0:
                        for e in enemies_hit:
                                 e.active = False # 小型敌机损毁
                                 
                    mid_enemies_hit = pygame.sprite.spritecollide(b,mid_enemies, False, pygame.sprite.collide_mask)
                    if mid_enemies_hit:
                         b.active = False
                         for m in mid_enemies_hit:
                            if m.active == True: 
                                 boss1_energy+=bullet_sh
                            if boss1_energy == 0:
                                 m.active = False # 小型敌机损毁
                                 djpoint+=500
                    big_enemies_hit = pygame.sprite.spritecollide(b,big_enemies, False, pygame.sprite.collide_mask)
                    if big_enemies_hit:
                         b.active = False
                         for m in big_enemies_hit:
                            if m.active == True: 
                                 boss1_energy+=bullet_sh
                            if boss1_energy == 0:
                                 m.active = False # 小型敌机损毁
                                 djpoint+=1000
                                 running = False
##                                 return djpoint
                                 main3(djpoint)
                        
##                                   print (djpoint)#测试代码
        # 毁坏状态绘制爆炸的场面
        else:
            if not (delay % 3):
                screen.blit(our_plane.destroy_images[me_destroy_index], our_plane.rect)
                me_destroy_index = (me_destroy_index + 1) % 4
                if me_destroy_index == 0:
                    me_down_sound.play()
                    our_plane.reset()
                    screen.fill(color_white)
                    main3(djpoint)

        # 调用 pygame 实现的碰撞方法 spritecollide (我方飞机如果和敌机碰撞, 更改飞机的存活属性)
        enemies_down = pygame.sprite.spritecollide(our_plane, enemies, False, pygame.sprite.collide_mask)
        if enemies_down:
            our_plane.active = False
##                main()
            for row in enemies:
                row.active = False
                
                

        # 响应用户的操作
        for event in pygame.event.get():
            if event.type == 12:  # 如果用户按下屏幕上的关闭按钮，触发QUIT事件，程序退出
                pygame.quit()
                sys.exit()
                

        if delay == 0:
            delay = 60
        delay -= 1

        #显示得分
        surface1 = font1.render(u'%s'%djpoint, True, [255, 255, 255])
        surface2 = font.render(u'按M键来暂停游戏！', True, [255, 255, 255])
        screen.blit(surface1, [420, 20])
        screen.blit(surface2, [0,0])
        

        # 获得用户所有的键盘输入序列(如果用户通过键盘发出“向上”的指令,其他类似)
        key_pressed = pygame.key.get_pressed()
        if key_pressed[K_w] or key_pressed[K_UP]:
            our_plane.move_up()
        if key_pressed[K_s] or key_pressed[K_DOWN]:
            our_plane.move_down()
        if key_pressed[K_a] or key_pressed[K_LEFT]:
            our_plane.move_left()
        if key_pressed[K_d] or key_pressed[K_RIGHT]:
            our_plane.move_right()
        if key_pressed[K_m]:
            pause = True
            button_down_sound.play()
            paused(pause)
        # 绘制图像并输出到屏幕上面
        pygame.display.flip()


##if __name__ == '__main__':
##    main(0)

