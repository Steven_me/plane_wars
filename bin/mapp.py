import os
import sys
import pygame
from pygame.locals import *
# 地图
class GameBackground(object):
    # 初始化地图
    def __init__(self, scene):
        # 加载相同张图片资源,做交替实现地图滚动
        self.image1 = pygame.image.load("material/image/background.png")
        self.image2 = pygame.image.load("material/image/background.png")
        # 保存场景对象
        self.main_scene = scene
        # 辅助移动地图
        self.y1 = 0
        self.y2 = -self.main_scene.size[1]
 
    # 计算地图图片绘制坐标
    def action(self):
        self.y1 = self.y1 + 1
        self.y2 = self.y2 + 1
        if self.y1 >= self.main_scene.size[1]:
            self.y1 = 0
        if self.y2 >= 0:
            self.y2 = -self.main_scene.size[1]
 
    # 绘制地图的两张图片
    def draw(self):
        self.main_scene.scene.blit(self.image1, (0, self.y1))
        self.main_scene.scene.blit(self.image2, (0, self.y2))
class MainScene(object):
    # 初始化主场景
    def __init__(self):
        # 场景尺寸
        self.size = (480, 850)
        # 场景对象
        self.scene = pygame.display.set_mode([self.size[0], self.size[1]])
        # 设置标题
        self.map = GameBackground(self)
 
 
    # 绘制
    def draw_elements(self):
        self.map.draw()
 
    # 动作
    def action_elements(self):
        self.map.action()
 
    # 处理事件
    def handle_event(self):
        pass
 
    # 碰撞检测
    def detect_conlision(self):
        pass
 
    # 主循环,主要处理各种事件
    def run_scene(self):
 
            # 计算元素坐标
            self.action_elements()
            # 绘制元素图片
            self.draw_elements()
            # 处理事件
            self.handle_event()
            # 碰撞检测
            self.detect_conlision()
            
 

