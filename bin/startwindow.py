#! /usr/bin/env python
# -*- coding: utf-8 -*-
import pygame
import time
import sys
from main import *
from endwindow import *
from settings import *
from pygame.locals import *

pygame.init()
pygame.mixer.init()
bg_size = 480, 850  # 初始化游戏背景大小(宽, 高)
screen = pygame.display.set_mode(bg_size)  # 设置背景对话框
pygame.display.set_caption("飞机大战--Beta")  # 设置标题
background = pygame.image.load( "material/image/background.png")  # 加载背景图片,并设置为不透明
background=pygame.image.load(r"material/startwindow.png")
logoimage=pygame.image.load(r"material/logoimage.png")
def main0():
    font = pygame.font.SysFont('SimHei', 32, True)  # 设置使用系统字体（用于支持中文）
    antialias = True  # 设置抗锯齿（平滑文字）
    black = 0, 0, 0  # 设置文字颜色
    while 1:
        clock = pygame.time.Clock() # 微信的飞机貌似是喷气式的, 那么这个就涉及到一个帧数的问题
        clock.tick(60)
        screen.blit(background,(0,0))
        screen.blit(logoimage,(30,90))
        for event in pygame.event.get():
            pygame.display.update()
            time.sleep(0.01)
            if event.type == pygame.QUIT:
                    pygame.quit()
                    sys.exit()

            if event.type == pygame.MOUSEBUTTONDOWN:
                    main(0)
        
        text = font.render(u"单击屏幕开始游戏！", antialias, [0,0,0])
        text2 = font.render(u"提示：按上下左右键进行移动！", antialias, [0,0,0])
        text3 = font.render(u"挑战自我！一条命闯到底！", antialias, [0,0,0])
        screen.blit(text,[80, 350])# 屏幕填充文字
        screen.blit(text2,[20, 400])# 屏幕填充文字
        screen.blit(text3,[40, 450])# 屏幕填充文字
        pygame.display.flip()     # 刷新屏幕

main0()
