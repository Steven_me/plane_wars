# -*- coding: utf-8 -*-
from pygame.locals import *
import pygame
pygame.init()  # 游戏初始化
class State:
    """泛型游戏状态类，可以处理事件并在给定的表面上显示自身。"""

    def handle(self, event):
        """处理退出事件。"""
        if event.type == QUIT:
            sys.exit()
        if event.type == KEYDOWN and event.key == K_ESCAPE:
            sys.exit()

    def paused_display(self, screen):
        """暂停时显示。"""
        screen.fill((255,255,255))  # 屏幕填充颜色

    def display(self, screen):  # 默认不作处理，由子类Level重写。
        """刷新显示时。"""
        pass
class Paused(State):
    """暂停游戏的状态，按任意键或点击鼠标退出暂停状态。"""
    
    finish = False  # 记录是否完成按键或鼠标点击
    text = '按ESC键继续！'  # 存储屏幕中显示的文字
##    image = "material/image/btn_finish.png" # 存储屏幕中显示的图片
    image =None

    def handle(self, event):
        """处理按任意键继续游戏。"""
        State.handle(self, event)
        if event.type in [MOUSEBUTTONDOWN, KEYDOWN]:  # 如果捕获鼠标点击和按键的事件
            self.finish = True  # 记录完成操作

    def update(self, game):
        """按任意键时进入下一个游戏状态。"""
        if self.finish:  # 如果按键或鼠标点击完成
            game.next_state = self.next_state()  # 将变量next_state中类的实例化，存入Game类进行处理。

    def paused_display(self, screen):
        """暂停时显示的处理。"""

        State.paused_display(self, screen)  # 重载超类中的方法
        font = pygame.font.SysFont('SimHei', 32, True)  # 设置使用系统字体（用于支持中文）
        lines = self.text.strip().splitlines()  # 获取子类中设置的文字内容并分行
        height = len(lines) * font.get_linesize()  # 计算文字高度
        center, middle = screen.get_rect().center  # 获取屏幕中心坐标值
        top = middle - height // 2  # 计算显示内容中心点的y轴坐标
        if self.image:  # 如果有图片
            image = pygame.image.load("material/image/btn_finish.png").convert_alpha()  # 载入图片
            img_rect = image.get_rect()  # 创建矩形
            top += img_rect.height // 2  # 重新计算顶部y轴坐标
            if self.text:  # 如果有文字
                img_rect.midbottom = center, top - 20  # 设置图片位置为中心且底边与文字顶部间隔20像素。
            else:  # 否则
                img_rect.center = center, middle  # 设置图片中心点为屏幕中心点
            screen.blit(image, img_rect)  # 屏幕填充图片

        antialias = True  # 设置抗锯齿（平滑文字）
        black = 0, 0, 0  # 设置文字颜色
        for line in lines:
            text = font.render(line.strip(), antialias, black)  # 创建单行文字对象
            txt_rect = text.get_rect()  # 创建矩形
            txt_rect.midtop = center, top  # 设置文字显示位置
            screen.blit(text, txt_rect)  # 屏幕填充文字
            top += font.get_linesize()  # 逐行下移文字位置
        pygame.display.flip()  # 刷新屏幕显示
##class Info(Paused):
##    """游戏信息。"""
##    next_state = Level  # 将类存入变量，以便在Paused类中实例化。
##    text = '''控制移动你的飞机，
##           不要被敌机撞到它。'''
##
##class StartUp(Paused):
##    """进入游戏。"""
##    next_state = Info  # 将类存入变量，以便在Paused类中实例化。
##    if config.getint('Welcome', 'skin'):  # 读取配置文件中欢迎界面的方案设置
##        text = ''
##        image = config.get('Image', 'welcome')
##    else:
##        text = '老司机开飞机'
##        image = config.get('Image', 'plane')
##
##class LevelCleared(Paused):
##    """游戏过关。"""
##
##    def __init__(self, number):
##        """过关信息。"""
##        self.number = number  # 获取当前关卡级别
##        self.text = '''恭喜闯过第 %d 关，
##        点击继续下一关。''' % self.number
##
##    def next_state(self):
##        """创建下一关卡。"""
##        return Level(self.number + 1)  # 返回下移关卡的对象
##
##class GameOver(Paused):
##    """游戏结束。"""
##    next_state = Level  # 将类存入变量，以便在Paused类中实例化。
##    text = '游戏结束！'
